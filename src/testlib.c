#include <stdio.h>
#include <stdlib.h>
#ifdef WITH_LMDB
#include "lmdb.h"
#endif

#include "../include/testlib.h"

#ifdef WITH_LMDB
#define E(expr) CHECK((rc = (expr)) == MDB_SUCCESS, #expr)
#define CHECK(test, msg) ((test) ? (void)0 : ((void)fprintf(stderr, \
	"%s:%d: %s: %s\n", __FILE__, __LINE__, msg, mdb_strerror(rc)), abort()))
#endif

int testfn(char * s){

#ifdef WITH_LMDB

    int rc;
    MDB_env *env;

    // always first, create an environment, then open it
    E(mdb_env_create(&env));
    E(mdb_env_set_maxreaders(env, 1));
    E(mdb_env_set_mapsize(env, 10485760));

#endif

    printf("%s\n", s);
    return 0;
}